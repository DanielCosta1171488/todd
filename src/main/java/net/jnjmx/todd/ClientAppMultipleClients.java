package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import javax.management.remote.*;
		

public class ClientAppMultipleClients {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Todd ClientAppMultipleClients... Connecting multiple clients");

		try {
			String server = "192.168.33:11";
			Integer numberOfConnections = 5;
			if (args.length >= 1) {
				server = args[0];
			}
			if (args.length >= 2) {
				numberOfConnections = Integer.parseInt(args[1]);
			}

            System.out.println("Connecting to  TODD at "+server+" ...");
            System.out.println("Creating several sessions for a few seconds");

			List<Client> listOfClients = new ArrayList<Client>();

			for(int i=0; i<numberOfConnections; i++ ){
				listOfClients.add(new Client(server));
			}
			System.out.println("Created "+numberOfConnections+" session. Waiting 10 seconds before disconnect!");
            Thread.sleep(10000);

			for(Client c : listOfClients){
				c.close();
			}

			System.out.println("Created sessions disconnected");
			System.exit(0);
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
	}
}
