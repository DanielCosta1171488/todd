package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.management.Notification;
import javax.management.NotificationListener;

public class JMXNotificationListener implements NotificationListener {

	private void handleNotification(Notification notification, String notificationType, String hostName, String hostServiceName, String hostIp) {
		
        String returnCode;
        String message;
        String pathFile = "/tmp/test";

		if (notificationType.equals(notification.getType().toString())){
			// There is a shortage of resources.
			returnCode="2";
			message="Passive Check OVERLOAD RESOURCES.";
			System.out.println("Sending OVERLOAD notification.");
		} else {
			returnCode="0";
			message="Passive Check OK.";
			System.out.println("Sending OK notification.");
		}

		// Print message to file
		try {
			File file = new File (pathFile);
			file.getParentFile().mkdirs();
			PrintWriter writer = new PrintWriter(file, "UTF-8");
			writer.print(hostName+"\t"+hostServiceName+"\t"+returnCode+"\t"+message+"\n\n");
			writer.close();
		} catch (Exception e) {
			System.err.println("Exception when printing to the file.");
			System.exit(1);
		}

		// Execute the command send_nsca -H <hostname> < textfile
		String command = "send_nsca -H " + hostIp + " < " + pathFile;
		try {
			System.out.println("sending command: \n"+command);
			Process p = Runtime.getRuntime().exec(new String[] { "/bin/sh"
				, "-c", command });            
			BufferedReader br = new BufferedReader(
				new InputStreamReader(p.getInputStream()));
			String s;
			while ((s = br.readLine()) != null)
				System.out.println("line: " + s);
			p.waitFor();
			System.out.println ("exit: " + p.exitValue());
			p.destroy();
		} catch (Exception e) {
			System.err.println("Exception at executing command:\n" + command);
			System.exit(1);
		}
	}

	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println("Received Notification");
		System.out.println("======================================");
		System.out.println("Timestamp: " + notification.getTimeStamp());
		System.out.println("Type: " + notification.getType());
		System.out.println("Sequence Number: " + notification.getSequenceNumber());
		System.out.println("Message: " + notification.getMessage());
		System.out.println("User Data: " + notification.getUserData());
		System.out.println("Source: " + notification.getSource());
		System.out.println("======================================");

		if ("todd:id=PercentageOfResource".equals(notification.getSource().toString()) ) {
			handleNotification(notification, "jmx.monitor.gauge.low", "monitored-todd", "passive-todd-availabilityCheck", "192.168.33.11");
		}
	}

}

